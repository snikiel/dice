#!/usr/bin/env python

# WS client example

import asyncio
import json
import websockets

async def hello1():
    uri = "ws://localhost:6789"
    async with websockets.connect(uri) as websocket:
        action = input("localhost plus or minus? ")

        await websocket.send(json.dumps({'action': action}))
        print(f"> {action}")
        
async def hello2():
    uri = "ws://45.79.179.244:6789"
    async with websockets.connect(uri) as websocket:
        action = input("45.79.179.244 plus or minus? ")

        await websocket.send(json.dumps({'action': action}))
        print(f"> {action}")

asyncio.get_event_loop().run_until_complete(hello1())
asyncio.get_event_loop().run_until_complete(hello2())