#!/usr/bin/env python

# WS server example that synchronizes state across clients

import asyncio
import json
import logging
import random
import websockets

logging.basicConfig(
	filename="/var/www/scottnikiel.com/log/dice.log",
	level=logging.DEBUG,
	format='%(asctime)s %(message)s',
	datefmt='%m/%d/%Y %I:%M:%S %p')

FENG_SHUI_ROLLS = []
FUDGE_ROLLS = []
USERS = set()
USERDICT = {}


def users_event(name, uid, action, rolls, configuration=None):
    user_list = []
    feng_shui_rolls = rolls
    fudge_rolls = rolls
    if rolls:
        feng_shui_rolls = FENG_SHUI_ROLLS
        fudge_rolls = FUDGE_ROLLS
    for user in USERDICT.keys():
        if "username" in USERDICT[user].keys():
            user_list.append(USERDICT[user]["username"])
    return json.dumps({"type": "users", "action": action, "count": len(USERS), "name": name, "uid": uid, "user_list": user_list, "feng_shui_rolls": feng_shui_rolls, "fudge_rolls": fudge_rolls, "configuration": configuration})

def feng_shui_roll_event(name, type, configuration):
    if type == "clear-data":
        global FENG_SHUI_ROLLS
        FENG_SHUI_ROLLS = []
        print(f"Clearing data requested by [{name}]")
        return json.dumps({"type": type, "pos": 0, "neg": 0, "fortune": 0, "name": name, "configuration": configuration})

    random.seed()
    pos = random.randrange(1,7)
    neg = random.randrange(1,7) if type != "initiative" else 0
    fortune = random.randrange(1,7) if type == "fortune" else 0
    FENG_SHUI_ROLLS.append(["roll", name, pos, neg, fortune])
    return json.dumps({"type": type, "pos": pos, "neg": neg, "fortune": fortune, "name": name, "configuration": configuration})

def feng_shui_reroll_event(name, type, configuration):
    random.seed()
    pos = random.randrange(1,7) if type != "neg" else 0
    neg = random.randrange(1,7) if type != "pos" else 0
    FENG_SHUI_ROLLS.append(["reroll", name, pos, neg, 0])
    return json.dumps({"type": "reroll", "pos": pos, "neg": neg, "fortune": 0, "name": name, "configuration": configuration})

def fudge_roll_event(name, type, configuration, auto_success, rerolls, reroll_zeros):
    if type == "clear-data":
        global FUDGE_ROLLS
        FUDGE_ROLLS = []
        print(F"Clearing data requested by [{name}]")
        return json.dumps({"type": type, "fudge_rolls": [], "name": name, "configuration": configuration})
        
    if auto_success is None:
        auto_success = 0
    if isinstance(auto_success,str):
        auto_success = int(auto_success)

    random.seed()
    rolls = []
    for _ in range(0,4):
        num = [random.randrange(-1,2)]
        if num[0] < 1 and auto_success > 0:
            num.append(1)
            auto_success -= 1
        rolls.append(num)
    
    FUDGE_ROLLS.append(["roll", name, rolls])
    return json.dumps({"type": "roll", "fudge_rolls": rolls, "name": name, "configuration": configuration, "rerolls": rerolls})

def fudge_reroll_event(name, type, configuration, reroll_type, rerolls, reroll_zeros):
    global FUDGE_ROLLS

    previous_rolls = []
    for r in FUDGE_ROLLS:
        if r[0] == "roll" and r[1] == name:
            previous_rolls = r[2]
            
    if rerolls is None:
        rerolls = 0
    if isinstance(rerolls,str):
        if rerolls == "":
            rerolls = 0
        else:
            rerolls = int(rerolls)

    random.seed()
    rolls = []
    if reroll_type == "stat" and rerolls > 0:
        for r in previous_rolls:
            logging.info(f"r [{r}]")
            if r[len(r) - 1] < 0 and rerolls > 0:
                rolls.append([random.randrange(-1,2)])
                rerolls -= 1
            else:
                rolls.append([])
        if reroll_zeros:
            for i in range(0,4):
                r = previous_rolls[i]
                r = r[len(r) - 1]
                if r < 1 and rerolls > 0:
                    rolls[i] = [random.randrange(-1,2)]
                    rerolls -= 1
    elif reroll_type == "crit+":
        for _ in range(0,4):
            rolls.append([random.randrange(-1,2)])
    elif reroll_type == "crit-":
        for _ in range(0,4):
            rolls.append([random.randrange(-1,2)])

    FUDGE_ROLLS.append(["roll", name, rolls])
    return json.dumps({"type": "reroll", "fudge_rolls": rolls, "name": name, "configuration": configuration, "rerolls": rerolls, "reroll_type": reroll_type})


async def notify_users(name=None, uid=None, action=None, configuration=None, rolls=None):
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = users_event(name, uid, action, rolls, configuration)
        await asyncio.wait([user.send(message) for user in USERS])

async def notify_roll(name, configuration, type, reroll_type=None, auto_success=0, rerolls=0, reroll_zeros=False):
    if USERS:
        message = ""
        if configuration == "feng-shui":
            message = feng_shui_reroll_event(name, reroll_type, configuration) if reroll_type else feng_shui_roll_event(name, type, configuration)
        elif configuration == "fudge":
            message = fudge_reroll_event(name, type, configuration, reroll_type, rerolls,reroll_zeros) if reroll_type else fudge_roll_event(name, type, configuration, auto_success, rerolls, reroll_zeros)
        await asyncio.wait([user.send(message) for user in USERS])

async def register(websocket):
    USERDICT[websocket] = {}
    USERS.add(websocket)
    await notify_users(None, None, None, None, True)


async def unregister(websocket):
    uid = USERDICT[websocket]["uid"]
    USERS.remove(websocket)
    del USERDICT[websocket]
    await notify_users(None, uid, "remove")


async def router(websocket, path):
    # register(websocket) sends user_event() to websocket
    try:
        await register(websocket)
        try:
            async for message in websocket:
                data = json.loads(message)
                if "configuration" not in data.keys():
                    logging.info(f"data: [{data}]")
                    data["configuration"] = None
                if "auto_success" not in data.keys():
                    data["auto_success"] = None
                if "rerolls" not in data.keys():
                    data["rerolls"] = None
                if "reroll_zeros" not in data.keys():
                    data["reroll_zeros"] = None

                if data["action"] == "username":
                    USERDICT[websocket]["uid"] = data["uid"]
                    USERDICT[websocket]["username"] = data["username"]
                    await notify_users(data["username"], data["uid"], None, data["configuration"])
                elif data["action"] == "roll":
                    await notify_roll(data["username"], data["configuration"],"roll", None, data["auto_success"], data["rerolls"], data["reroll_zeros"])
                elif data["action"] == "roll-fortune":
                    await notify_roll(data["username"], data["configuration"],"fortune")
                elif data["action"] == "roll-initiative":
                    await notify_roll(data["username"], data["configuration"],"initiative")
                elif data["action"] == "reroll":
                    await notify_roll(data["username"], data["configuration"],"reroll", data["type"], data["auto_success"], data["rerolls"], data["reroll_zeros"])
                elif data["action"] == "clear-data":
                    await notify_roll(data["username"], data["configuration"], "clear-data")
                else:
                    logging.error(f"unsupported event: {data}")
        finally:
            await unregister(websocket)

    except websockets.exceptions.ConnectionClosed as e:
        print(f"websocket closed: {e}")
        unregister(websocket)

start_server = websockets.serve(router, "localhost", 6767)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()