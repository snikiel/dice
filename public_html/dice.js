var copyright_year = document.querySelector('#cr-year'),
    users = document.querySelector('.users'),
    user_names = document.querySelector('.user_names'),
    username = document.querySelector('#USERNAME'),
    dice_configuration = document.querySelector('#CONFIG'),
    feng_shui_dice = document.querySelector('#feng-shui-dice'),
    fudge_dice = document.querySelector('#fudge-dice'),
    feng_shui_dice_buttons = document.querySelector('#feng-shui-dice-buttons')
    fudge_dice_buttons = document.querySelector('#fudge-dice-buttons')
    feng_shui_history_head = document.querySelector('#feng-shui-history-head')
    fudge_history_head = document.querySelector('#fudge-history-head')
    pos_die = document.querySelector('#POS.die'),
    neg_die = document.querySelector('#NEG.die'),
    fortune_die = document.querySelector('#FORTUNE.die'),
    fudge_die_0 = document.querySelector('#FUDGE0'),
    fudge_die_1 = document.querySelector('#FUDGE1'),
    fudge_die_2 = document.querySelector('#FUDGE2'),
    fudge_die_3 = document.querySelector('#FUDGE3'),
    fudge_dice_list = [fudge_die_0,fudge_die_1,fudge_die_2,fudge_die_3],
    name_requireds = document.querySelectorAll('.name-required'),
    name_prompt = document.querySelector('#name-prompt'),
    roll = document.querySelector('#ROLL'),
    roll_with_fortune = document.querySelector('#ROLL_WITH_FORTUNE'),
    roll_initiative = document.querySelector('#ROLL_INITIATIVE'),
    reroll = document.querySelector('#REROLL'),
    fudge_roll = document.querySelector('#FUDGE_ROLL'),
    fudge_reroll = document.querySelector('#FUDGE_REROLL'),
    feng_shui_roll_history = document.querySelector('#roll-history'),
    fudge_roll_history = document.querySelector('#fudge-roll-history'),
    clear_data = document.querySelector('#CLEAR_DATA'),
    uid = Math.random().toString(36).replace(/[^a-z0-9]+/g, '').substr(1,10),
    ordinals = ["","first","second","third","fourth","fifth","fourth"], // Yes... the sixth ordinal uses the same styling as the fourth.
    system_sections = {
        'feng-shui' : [feng_shui_dice, feng_shui_dice_buttons, feng_shui_history_head, feng_shui_roll_history],
        'fudge': [fudge_dice, fudge_dice_buttons, fudge_history_head, fudge_roll_history]
    };

function add_dot(node) {
    var dot = document.createElement('span');
    dot.classList.add("dot");
    node.appendChild(dot);
}
function build_die_column(num,node) {
    var div = document.createElement('div');
    div.classList.add('die-column');
    for (var i = 0; i < num; i++) {
        add_dot(div)
    }
    node.appendChild(div);
}
function build_die(num,die) {
    die.classList.add(ordinals[num] + "-face");
    if (num <= 3) {
        for (var i = 0; i < num; i++) {
            add_dot(die);
        }
    }
    else if (num == 4) {
        build_die_column(2, die);
        build_die_column(2, die);
    }
    else if (num == 5) {
        build_die_column(2, die);
        build_die_column(1, die);
        build_die_column(2, die);
    }
    else if (num == 6) {
        build_die_column(3, die);
        build_die_column(3, die);
    }
}

function fudge_build_die(num,die) {
    die.classList.add('fudge-die');
    fill = ""
    if(num.length > 1)
        fill = 'fill="red"'
    if (num[num.length - 1] == 1)
        die.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path ' + fill + ' d="M24 9h-9v-9h-6v9h-9v6h9v9h6v-9h9z"/></svg>';
    else if (num[num.length - 1] == -1)
        die.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path ' + fill + ' d="M0 9h24v6h-24z"/></svg>';
    else 
        die.innerHTML = "";

    if (num.length == 0)
        die.setAttribute("style","opacity: 0.1;");
    
}

function fudge_update_rerolls(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    var new_rerolls = (data.rerolls == 0) ? "" : data.rerolls;
    if (data.reroll_type == "crit+" || data.reroll_type == "crit-")
        new_rerolls = 0

    document.getElementById("FUDGE_REROLLS").value = new_rerolls;
}

function reset_buttons(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    document.getElementById("REROLL").setAttribute("style","display: none;");
    document.getElementById("ROLL").removeAttribute("disabled");
    document.getElementById("ROLL_WITH_FORTUNE").removeAttribute("disabled");
    document.getElementById("ROLL_INITIATIVE").removeAttribute("disabled");
    username.removeAttribute("disabled");
}

function fudge_reset_buttons(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    document.getElementById("FUDGE_REROLL").setAttribute("style","display: none;");
    document.getElementById("FUDGE_ROLL").removeAttribute("disabled");
    username.removeAttribute("disabled");
}

function clear_dice(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    pos_die.removeAttribute("style");
    pos_die.setAttribute("class","die");
    pos_die.innerHTML = "";
    neg_die.removeAttribute("style");
    neg_die.setAttribute("class","die");
    neg_die.innerHTML = "";
    fortune_die.removeAttribute("style");
    fortune_die.setAttribute("class","die");
    fortune_die.innerHTML = "";
}

function fudge_clear_dice(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;

        for (var i = 0; i < 4; i++) {
            fudge_dice_list[i].removeAttribute("style");
            fudge_dice_list[i].setAttribute("class", "die");
            fudge_dice_list[i].innerHTML = "";
        };
}

function set_dice(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    if (data.pos == 0)
        pos_die.setAttribute("style","opacity: 0.1;");
    if (data.neg == 0)
        neg_die.setAttribute("style","opacity: 0.1;");
    if (data.fortune == 0)
        fortune_die.setAttribute("style","opacity: 0.1;");
    build_die(data.pos, pos_die);
    build_die(data.neg, neg_die);
    build_die(data.fortune, fortune_die);
}
function fudge_set_dice(action_username) {
    if (action_username != document.getElementById("USERNAME").value)
        return;

    for (var i = 0; i < 4; i++) {
        fudge_build_die(data.fudge_rolls[i], fudge_dice_list[i]);
    };
}

function setup_reroll(action_username, type, pos, neg) {
    if (action_username != document.getElementById("USERNAME").value)
        return;
    var reroll_type = (pos == 6 && neg == 6) ? "boxcars" : ((pos == 6) ? "pos" : ((neg == 6) ? "neg" : false));
    if (reroll_type && type != "initiative") {
        var button = document.getElementById("REROLL")
        var button_label = "Reroll 6";
        if (reroll_type == 'boxcars')
            button_label = "Reroll Boxcars!";
        button.setAttribute("value",button_label);
        button.setAttribute("reroll-type",reroll_type);
        button.removeAttribute("style");
        document.getElementById("ROLL").setAttribute("disabled","");
        document.getElementById("ROLL_WITH_FORTUNE").setAttribute("disabled","");
        document.getElementById("ROLL_INITIATIVE").setAttribute("disabled","");
        username.setAttribute("disabled","");
    }
}

function fudge_setup_reroll(action_username, type, rolls, rerolls) {
    if (action_username != document.getElementById("USERNAME").value)
        return;

    var combined = 0;
    rolls.forEach(r => { combined += r[0]; });
    var reroll_type = (Math.abs(combined) != 4)
        ? ((rerolls > 0) ? 'stat' : false)
        : ((combined < 0) ? 'crit-' : 'crit+');

    if (reroll_type) {
        var button = document.getElementById("FUDGE_REROLL")
        var button_label = "Critical, Roll again!";
        if (reroll_type == 'stat')
            button_label = "Reroll up to " + rerolls + " dice";
        button.setAttribute("value",button_label);
        button.setAttribute("reroll-type",reroll_type);
        button.removeAttribute("style");
        document.getElementById("FUDGE_ROLL").setAttribute("disabled","");
        username.setAttribute("disabled","");
    }
}

function get_data_from_cookies() {
    document.cookie.split(';').forEach(e => {
        var key = e.split('=')[0];
        var value = e.split('=')[1];
        if (key.trim() == "uid")
            uid = value;
        else if (key.trim() == "username")
            username.value = value;
        else if (key.trim() == "admin")
            document.getElementById('ADMIN').removeAttribute("style");
    });
}

function trigger_namechange() {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    if (uname) {
        name_requireds.forEach(e => e.removeAttribute("style"));
        name_prompt.setAttribute("style", "display: none;");
        document.cookie = "username=" + uname + "; SameSite=Strict";
        document.cookie = "uid=" + uid + "; SameSite=Strict";
    }
    else {
        name_prompt.removeAttribute("style");
    }
    console.log("configuration: ", configuration);
    websocket.send(JSON.stringify({action: 'username', uid: uid, username: uname, configuration: configuration}));
}

function trigger_systemchange() {
    var system = document.getElementById("CONFIG").value;
    for (var s in system_sections) {
        if (s == system) 
            system_sections[s].forEach(element => element.removeAttribute("style"))
        else
            system_sections[s].forEach(element => element.setAttribute("style", "display: none;"));
    }
}

copyright_year.textContent = (new Date().getFullYear());

function nextBy_(attribute, node, value) {
    while (node = node.nextSibling) {
        if (node.getAttribute(attribute) == value)
            return node;
    }
    return null;
};

function createRow(values) {
    var type = values.shift();
    var parent = feng_shui_roll_history.firstChild;
    var row_class = (type == "roll") ? "parent" : "child";

    if (type == "reroll") {
        if (parent.getAttribute("username") != values[0]) {
            parent = nextBy_("username", parent, values[0])
        }
        var parent_name = parent.firstChild;
        var parent_total = parent_name.nextSibling;
        if (parent_name.getAttribute("rowspan")) {
            parent_name.setAttribute("rowspan", (parseInt(parent_name.getAttribute("rowspan")) + 1));
            parent_total.setAttribute("rowspan", (parseInt(parent_total.getAttribute("rowspan")) + 1));
        }
        else {
            parent_name.setAttribute("rowspan",2);
            parent_total.setAttribute("rowspan",2);
        }
        var parent_value = parseInt(parent_total.textContent);
        var recalced_total = parent_value + values[1] - values[2];
        parent_total.textContent = (recalced_total);
        parent_total.setAttribute("class",((recalced_total > 0) ? "pos" : ((recalced_total < 0) ? "neg": "zero")));
    }

    var tr = document.createElement("tr");
    tr.setAttribute("class",row_class);
    tr.setAttribute("username",values[0]);
    var fortune = document.createElement("td");
    fortune.textContent = ((values[3]) > 0 ? values[3] : "");
    if (type != "reroll") {
        var user = document.createElement("th");
        user.textContent = (values[0]);
        tr.appendChild(user);

        var total = document.createElement("td");
        var combined = values[1] - values[2] + values[3];
        total.textContent = (combined);
        total.setAttribute("class",((combined > 0) ? "pos" : ((combined < 0) ? "neg": "zero")));
        tr.appendChild(total);
    }
    
    var pos = document.createElement("td");
    pos.textContent = ((values[1] > 0) ? values[1] : "");
    var neg = document.createElement("td");
    neg.textContent = ((values[2] > 0) ? values[2] : "");
    if (values[1] == 6 && values[2] == 6) {
        neg.classList.add("boxcars");
        pos.classList.add("boxcars");
    }
    tr.appendChild(pos);
    tr.appendChild(neg);
    tr.appendChild(fortune);

    if (type == "reroll") {
        if (feng_shui_roll_history.querySelectorAll(".parent").length > 1) {
            var next_parent = nextBy_("class", parent, "parent");
            feng_shui_roll_history.insertBefore(tr, next_parent);
        }
        else
        feng_shui_roll_history.appendChild(tr);
    }
    else {
        feng_shui_roll_history.insertBefore(tr, feng_shui_roll_history.firstChild);
    }
}

function fudge_createRow(values) {
    var type = values.shift();
    var name = values.shift();
    var reroll_type = values.shift();
    var parent = fudge_roll_history.firstChild;
    var row_class = (type == "roll") ? "parent" : "child";

    if (type == "reroll") {
        if (parent.getAttribute("username") != name) {
            parent = nextBy_("username", parent, name)
        }
        var parent_name = parent.firstChild;
        var parent_total = parent_name.nextSibling;
        if (parent_name.getAttribute("rowspan")) {
            parent_name.setAttribute("rowspan", (parseInt(parent_name.getAttribute("rowspan")) + 1));
            parent_total.setAttribute("rowspan", (parseInt(parent_total.getAttribute("rowspan")) + 1));
        }
        else {
            parent_name.setAttribute("rowspan",2);
            parent_total.setAttribute("rowspan",2);
        }

        var parent_value = parseInt(parent_total.textContent);
        var recalced_total = parent_value;
        if (reroll_type == "stat") {
            recalced_total = 0;
            for (var i = 0; i < 4; i++) {
                v = values[0][i];
                if (v[v.length - 1] !== undefined)
                    recalced_total += v[v.length -1];
                else {
                    var first_row = parent.children[i + 2].children;
                    var first_row_val = first_row[first_row.length -1].innerHTML
                    var parsed_val = parseInt(first_row_val);
                    recalced_total += parsed_val;
                }
            };
        }
        else if (reroll_type == "crit+")
        {
            values[0].forEach(v => {
                if (v > 0)
                    recalced_total += v[v.length -1];
            });
        }
        else if (reroll_type == "crit-") {
            values[0].forEach(v => {
                if (v < 0)
                    recalced_total += v[v.length -1];
            });
        }
        parent_total.textContent = (recalced_total);
        parent_total.setAttribute("class",((recalced_total > 0) ? "pos" : ((recalced_total < 0) ? "neg": "zero")));
    }

    var tr = document.createElement("tr");
    tr.setAttribute("class",row_class);
    tr.setAttribute("username",name);


    if (type != "reroll") {
        var user = document.createElement("th");
        user.textContent = (name);
        tr.appendChild(user);
        
        var total = document.createElement("td");
        var combined = 0;
        values[0].forEach(v => { combined += v[v.length -1]; });
        total.textContent = (combined);
        total.setAttribute("class",((combined > 0) ? "pos" : ((combined < 0) ? "neg": "zero")));
        tr.appendChild(total);
    }
    
    values[0].forEach(roll => {
        var row = document.createElement("td");
        roll.forEach(r => {
            var span = document.createElement("span");
            span.setAttribute("class", "fudge-die-roll");
            span.textContent = r;
            row.appendChild(span);
        });
        tr.appendChild(row);
    });

    if (type == "reroll") {
        if (fudge_roll_history.querySelectorAll(".parent").length > 1) {
            var next_parent = nextBy_("class", parent, "parent");
            fudge_roll_history.insertBefore(tr, next_parent);
        }
        else
        fudge_roll_history.appendChild(tr);
    }
    else {
        fudge_roll_history.insertBefore(tr, fudge_roll_history.firstChild);
    }
}

function smoothscroll(){
    var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    if (currentScroll > 0) {
         window.requestAnimationFrame(smoothscroll);
         window.scrollTo (0,currentScroll - (currentScroll/5));
    }
}

username.onchange = function (event) {
    trigger_namechange();
    if (window.innerWidth < 768)
        smoothscroll();
};
dice_configuration.onchange = function (event) {
    trigger_systemchange();
}
roll.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    websocket.send(JSON.stringify({action: 'roll', fortune: false, uid: uid, username: uname, configuration: configuration}));
};
roll_with_fortune.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    websocket.send(JSON.stringify({action: 'roll-fortune', uid: uid, username: uname, configuration: configuration}));
};
roll_initiative.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    websocket.send(JSON.stringify({action: 'roll-initiative', uid: uid, username: uname, configuration: configuration}));
};
reroll.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    var type = document.getElementById("REROLL").getAttribute("reroll-type");
    websocket.send(JSON.stringify({action: 'reroll', type: type, uid: uid, username: uname, configuration: configuration}));
};
fudge_roll.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    rerolls = document.getElementById("FUDGE_REROLLS").value;
    if (rerolls == "" || isNaN(rerolls))
        rerolls = 0;
    reroll_zeros = document.getElementById("FUDGE_REROLL_ZEROS").checked;
    auto_success = document.getElementById("AUTO_SUCCESS").value;
    if (auto_success == "" || isNaN(auto_success))
        auto_success = 0;
    document.getElementById("AUTO_SUCCESS").value = "";
    websocket.send(JSON.stringify({action: 'roll', fortune: false, uid: uid, username: uname, configuration: configuration, auto_success: auto_success, rerolls: rerolls, reroll_zeros: reroll_zeros}));
};
fudge_reroll.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    type = document.getElementById("FUDGE_REROLL").getAttribute("reroll-type");
    rerolls = document.getElementById("FUDGE_REROLLS").value;
    websocket.send(JSON.stringify({action: 'reroll', type: type, uid: uid, username: uname, configuration: configuration, rerolls: rerolls}));
};
clear_data.onclick = function (event) {
    uname = document.getElementById("USERNAME").value;
    configuration = document.getElementById("CONFIG").value;
    websocket.send(JSON.stringify({action: "clear-data", uid: uid, username: uname, configuration: configuration}))
}


var websocket;
function connect() {
    if (window.location.href.startsWith("http"))
        websocket = new WebSocket('wss://nikiel.dev/websocket/dice/');
    else
        websocket = new WebSocket("ws://localhost:6767/");
    
    websocket.onclose = function(e) {
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function() {
        connect();
        }, 1000);
    };
    
    websocket.onerror = function(err) {
        console.error('Socket encountered error: ', err.message, 'Closing socket');
        websocket.close();
    };

    websocket.onmessage = function (event) {
        data = JSON.parse(event.data);
        switch (data.type) {
            case 'users':
                users.textContent = (data.count.toString());

                user_names.textContent = ("");
                if (data.user_list.length > 0) {
                    data.user_list.sort().forEach(e => {
                        var li = document.createElement('li');
                        li.textContent = (e ? e : "unnamed");
                        user_names.appendChild(li);
                    });
                }

                if (data.feng_shui_rolls)
                    data.feng_shui_rolls.forEach(e => {createRow(e);});

                break;
            case 'clear-data':
                if (data.configuration == "feng-shui")
                    feng_shui_roll_history.textContent = "";
                else if (data.configuration == "fudge")
                    fudge_roll_history.textContent = "";
                break;
            case 'roll':
            case 'fortune':
            case 'initiative':
                if (data.configuration == "feng-shui") {
                    reset_buttons(data.name);
                    clear_dice(data.name);
                    set_dice(data.name);

                    createRow(["roll", data.name, data.pos, data.neg, data.fortune]);

                    setup_reroll(data.name, data.type, data.pos, data.neg);
                }
                else if (data.configuration == "fudge") {
                    fudge_update_rerolls(data.name);
                    fudge_reset_buttons(data.name);
                    fudge_clear_dice(data.name);
                    fudge_set_dice(data.name);
                    fudge_createRow(["roll", data.name, "", data.fudge_rolls]);

                    fudge_setup_reroll(data.name, data.type, data.fudge_rolls, data.rerolls);
                }
                break;
            case 'reroll':
                if (data.configuration == "feng-shui") {
                    reset_buttons(data.name);
                    clear_dice(data.name);
                    set_dice(data.name);

                    createRow(["reroll", data.name, data.pos, data.neg, data.fortune]);

                    setup_reroll(data.name, data.type, data.pos, data.neg);
                }
                else if (data.configuration == "fudge") {
                    fudge_update_rerolls(data.name);
                    fudge_reset_buttons(data.name);
                    fudge_clear_dice(data.name);
                    fudge_set_dice(data.name);
                    fudge_createRow(["reroll", data.name, data.reroll_type, data.fudge_rolls]);

                    fudge_setup_reroll(data.name, data.type, data.fudge_rolls, data.rerolls);
                }
                break;
            default:
                console.error("unsupported event", data);
        }
    };

    get_data_from_cookies();
    if (uid && document.getElementById("USERNAME"))
        websocket.onopen = function(event) {trigger_namechange();}
}
connect();
